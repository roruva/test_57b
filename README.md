# test_57b

Test 1 and 3 57 Blocks
## Screenshots
### Screenshots
Inicial state
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_01.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_02.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
</div>
likes
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_03.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_04.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
</div>
Dislike
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_05.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
</div>
Video detail and MiniBar
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_06.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_07.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
</div>
Other tabs views
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_08.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_09.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_10.jpg" alt="Youtube app" height="400" style="margin-right: 5px"/>
</div>
Searchbox
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_11.jpg" alt="Searchbox" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_12.jpg" alt="Searchbox" height="400" style="margin-right: 5px"/>
  <img src="screenshots/flutter_13.jpg" alt="Searchbox" height="400" style="margin-right: 5px"/>
</div>

Video
<div style="display:flex;flex-direction:row;">
  <img src="screenshots/flutter_1.gif" alt="Paint App" height="400" style="margin-right: 5px"/>
</div>

### Run project
First of all download the dependencies:
```sh
flutter pub get
```
Then run on devices or simulator
```sh
flutter run
```

### Specs
These were the specifications used to build the project:\
\
**Flutter:** 1.22.0 \
**OS:** Mac OS X 10.15.5\
**dart:** 2.7.2
