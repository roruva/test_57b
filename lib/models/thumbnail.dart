part of models;

class Thumbnails {
  Thumbnails({this.url, this.width, this.height});

  Thumbnails.fromJson(Map<String, dynamic> json) {
    url = json['url'] as String;
    width = json['width'] as int;
    height = json['height'] as int;
  }

  String url;
  int width;
  int height;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic>{};
    data['url'] = url;
    data['width'] = width;
    data['height'] = height;
    return data;
  }
}
