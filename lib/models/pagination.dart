part of models;

class Pagination {
  Pagination({this.pageSize, this.pageNumber});

  Pagination.fromJson(Map<String, int> json) {
    pageSize = json['pageSize'];
    pageNumber = json['pageNumber'];
  }

  int pageSize;
  int pageNumber;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic>{};
    data['pageSize'] = pageSize;
    data['pageNumber'] = pageNumber;
    return data;
  }
}
