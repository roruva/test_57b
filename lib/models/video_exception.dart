part of models;

class VideoException implements Exception {
  const VideoException(this.error);

  final String error;

  @override
  String toString() => 'Video Error: $error';
}
