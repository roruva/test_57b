part of models;

class Video {
  Video({
    this.id,
    this.thumbnails,
    this.title,
    this.publishedTimeText,
    this.durationTxt,
    this.visits,
    this.likes,
    this.dislikes,
    this.noMoreLikes,
    this.noMoreDislikes,
    this.owner,
    this.url,
    this.channelThumb,
    this.richThumbnail,
  });

  Video.fromJson(Map<String, dynamic> json) {
    id = json['id'] as String;
    thumbnails = json['thumbnails'] != null
        ? new Thumbnails.fromJson(json['thumbnails'])
        : null;
    title = json['title'] as String;
    publishedTimeText = json['publishedTimeText'] as String;
    durationTxt = json['durationTxt'] as String;
    visits = json['visits'] as int;
    likes = Random().nextInt(20000);
    dislikes = Random().nextInt(1000);
    noMoreLikes = false;
    noMoreDislikes = false;
    owner = json['owner'] as String;
    url = json['url'] as String;
    channelThumb = json['channelThumb'] != null
        ? new Thumbnails.fromJson(json['channelThumb'])
        : null;
    richThumbnail = json['richThumbnail'] != null
        ? new Thumbnails.fromJson(json['richThumbnail'])
        : null;
  }

  String id;
  Thumbnails thumbnails;
  String title;
  String publishedTimeText;
  String durationTxt;
  int visits;
  int likes;
  int dislikes;
  bool noMoreLikes;
  bool noMoreDislikes;
  String owner;
  String url;
  Thumbnails channelThumb;
  Thumbnails richThumbnail;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    if (thumbnails != null) {
      data['thumbnails'] = thumbnails.toJson();
    }
    data['title'] = title;
    data['publishedTimeText'] = publishedTimeText;
    data['durationTxt'] = durationTxt;
    data['visits'] = visits;
    data['likes'] = likes;
    data['dislikes'] = dislikes;
    data['noMoreLikes'] = noMoreLikes;
    data['noMoreDislikes'] = noMoreDislikes;
    data['owner'] = owner;
    if (channelThumb != null) {
      data['channelThumb'] = channelThumb.toJson();
    }
    if (richThumbnail != null) {
      data['richThumbnail'] = richThumbnail.toJson();
    }
    return data;
  }

  String get description => '$owner • $visits • $publishedTimeText';
  String get likesString => formatNum(likes);
}
