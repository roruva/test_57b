/// [flutter-parts]
import 'package:flutter/material.dart';

/// [third-parts]
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// [app-parts]
import 'views/views.dart';

void main() {
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '57 Blocks Test',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: YoutubeView(),
    );
  }
}
