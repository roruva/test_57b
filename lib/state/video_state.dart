/// [third-packages]
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// [app-packages]
import 'package:test_57b/models/models.dart';
import 'package:test_57b/state/providers.dart';
import 'package:test_57b/utils/utils.dart';

/// [naming] Ntfr = Notifier

class VideoListNtfr extends StateNotifier<AsyncValue<List<Video>>> {
  VideoListNtfr(this.read, [AsyncValue<List<Video>> videoList])
      : super(videoList ?? AsyncValue.loading()) {
    getVideos();
  }

  final Reader read;

  Future<void> getVideos() async {
    try {
      Pagination pagination = read(paginationPvdr).state;

      List<Video> videos =
          await Requester.fetchPage(pagination.pageSize, pagination.pageNumber);

      List<Video> prevState =
          state.whenData((prevVideos) => prevVideos).data?.value ?? [];

      state = AsyncValue.data([...prevState, ...videos]);
      read(paginationPvdr).update();
      read(videoStatePvdr).isLoading(false);
    } on VideoException catch (e, st) {
      state = AsyncValue.error(e, st);
    }
  }

  void giveLike(String id) {
    state = state.whenData((List<Video> videos) => <Video>[
          for (final video in videos)
            if (video.id == id)
              Video(
                id: video.id,
                thumbnails: video.thumbnails,
                title: video.title,
                publishedTimeText: video.publishedTimeText,
                durationTxt: video.durationTxt,
                visits: video.visits,
                likes: video.likes + 1,
                dislikes:
                    video.noMoreDislikes ? video.dislikes - 1 : video.dislikes,
                noMoreLikes: true,
                noMoreDislikes: video.noMoreDislikes
                    ? !video.noMoreDislikes
                    : video.noMoreDislikes,
                owner: video.owner,
                channelThumb: video.channelThumb,
                richThumbnail: video.richThumbnail,
              )
            else
              video,
        ]);
  }

  void giveDislike(String id) {
    state = state.whenData((List<Video> videos) => <Video>[
          for (final video in videos)
            if (video.id == id)
              Video(
                id: video.id,
                thumbnails: video.thumbnails,
                title: video.title,
                publishedTimeText: video.publishedTimeText,
                durationTxt: video.durationTxt,
                visits: video.visits,
                likes: video.noMoreLikes ? video.likes - 1 : video.likes,
                dislikes: video.dislikes + 1,
                noMoreLikes:
                    video.noMoreLikes ? !video.noMoreLikes : video.noMoreLikes,
                noMoreDislikes: true,
                owner: video.owner,
                channelThumb: video.channelThumb,
                richThumbnail: video.richThumbnail,
              )
            else
              video,
        ]);
  }
}

class VideoStateModel {
  const VideoStateModel({this.videoSelected, this.isLoading});

  final Video videoSelected;
  final bool isLoading;
}

class VideoStateNtfr extends StateNotifier<VideoStateModel> {
  VideoStateNtfr() : super(_initialValue);
  static const _initialValue =
      VideoStateModel(isLoading: false, videoSelected: null);

  void choseVideo(Video video) {
    state = VideoStateModel(
      videoSelected: video,
      isLoading: state.isLoading,
    );
  }

  void isLoading(bool isLoading) {
    state = VideoStateModel(
      videoSelected: state.videoSelected,
      isLoading: isLoading,
    );
  }
}
