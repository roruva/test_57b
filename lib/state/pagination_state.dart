/// [third-packages]
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// [app-packages]
import 'package:test_57b/models/models.dart';

/// [naming] Ntfr = Notifier

class PaginationNtfr extends StateNotifier<Pagination> {
  PaginationNtfr() : super(_initialValue);

  static Pagination _initialValue = Pagination(pageNumber: 0, pageSize: 4);

  void update() {
    int pageNumber = state.pageNumber < 7 ? state.pageNumber + 1 : 0;
    state = Pagination(pageSize: state.pageSize, pageNumber: pageNumber);
  }
}
