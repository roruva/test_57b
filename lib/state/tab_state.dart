/// [third-packages]
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// [naming] Ntfr = Notifier

class TabNtfr extends StateNotifier<int> {
  TabNtfr() : super(0);

  void onChange(int index) {
    state = index;
  }
}
