/// [third-packages]
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// [app-packages]
import 'package:test_57b/models/models.dart';
import 'package:test_57b/utils/utils.dart';

/// [lib-parts]
import './pagination_state.dart';
import './video_state.dart';
import './tab_state.dart';

final paginationPvdr =
    StateNotifierProvider<PaginationNtfr>((ref) => PaginationNtfr());

final videoListPvdr = FutureProvider<List<Video>>((ref) async {
  final paginantion = ref.watch(paginationPvdr.state);
  return Requester.fetchPage(paginantion.pageSize, paginantion.pageSize);
});

final videosFuturePvdr =
    FutureProvider.family<List<Video>, String>((ref, query) async {
  return Requester.searchVideos(query);
});

final videoPvdr =
    StateNotifierProvider<VideoListNtfr>((ref) => VideoListNtfr(ref.read));

final videoStatePvdr = StateNotifierProvider((ref) => VideoStateNtfr());
final tabStatePvdr = StateNotifierProvider((ref) => TabNtfr());
