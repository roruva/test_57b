library constants;

/// [flutter-packages]
import 'package:flutter/material.dart';

/// [lib-parts]
part './colors.dart';
part './typography.dart';
