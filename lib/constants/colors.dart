part of constants;

// Youtube
Color youtubeRed = Color.fromRGBO(255, 0, 0, 1);
Color youtubeBlack = Color.fromRGBO(33, 33, 33, 1);

Color grey = youtubeBlack.withOpacity(0.6);
