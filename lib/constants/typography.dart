part of constants;

TextStyle p = TextStyle(color: youtubeBlack, fontSize: 12);

TextStyle videoTitle = TextStyle(
  color: youtubeBlack,
  fontSize: 14,
  fontWeight: FontWeight.w600,
);

TextStyle videoDesc = TextStyle(
  color: youtubeBlack.withOpacity(0.8),
  fontSize: 12,
);
