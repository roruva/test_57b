part of views.youtube;

class YoutubeView extends HookWidget {
  final List<Widget> tabs = <Widget>[
    HomeView(),
    ExploreView(),
    SubscriptionsView(),
    LibraryView(),
  ];

  void showSearchView(BuildContext context) {
    showSearch<dynamic>(
      context: context,
      delegate: DataSearch(label: 'Search...'),
    );
  }

  @override
  Widget build(BuildContext context) {
    int tabIndex = useProvider(tabStatePvdr.state);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: SvgPicture.asset(
          'assets/img/youtube_logo.svg',
          height: 24,
          fit: BoxFit.contain,
        ),
        iconTheme: IconThemeData(color: youtubeBlack),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.cast_outlined),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.notifications_outlined),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.search_outlined),
            onPressed: () {
              showSearchView(context);
            },
          ),
          IconButton(
            icon: Icon(Icons.account_circle_outlined),
            onPressed: () {},
          ),
        ],
      ),
      body: tabs[tabIndex],
      bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: true,
        showUnselectedLabels: true,
        currentIndex: tabIndex,
        selectedItemColor: youtubeRed,
        unselectedItemColor: youtubeBlack,
        onTap: (int newIndex) {
          context.read(tabStatePvdr).onChange(newIndex);
        },
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            activeIcon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.explore_outlined),
            activeIcon: Icon(Icons.explore),
            title: Text('Explore'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.subscriptions_outlined),
            activeIcon: Icon(Icons.subscriptions),
            title: Text('Subscription'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.video_library_outlined),
            activeIcon: Icon(Icons.video_library),
            title: Text('Library'),
          ),
        ],
      ),
    );
  }
}
