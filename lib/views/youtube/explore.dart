part of views.youtube;

class ExploreView extends StatelessWidget {
  const ExploreView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ColoredBox(
      color: Colors.indigo,
      child: SizedBox(
        width: size.width,
        height: size.height,
        child: Center(
          child: Icon(
            Icons.explore_rounded,
            size: 96,
            color: youtubeBlack.withOpacity(0.2),
          ),
        ),
      ),
    );
  }
}
