part of views.youtube;

class LibraryView extends StatelessWidget {
  const LibraryView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ColoredBox(
      color: Colors.purple,
      child: SizedBox(
        width: size.width,
        height: size.height,
        child: Center(
          child: Icon(
            Icons.video_library_outlined,
            size: 96,
            color: youtubeBlack.withOpacity(0.2),
          ),
        ),
      ),
    );
  }
}
