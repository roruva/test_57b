part of views.youtube;

/// [naming]
/// Ntfn = Notification

class HomeView extends HookWidget {
  const HomeView({Key key}) : super(key: key);

  void showVideoDetail(BuildContext context, Video video) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        builder: (BuildContext ctx) {
          EdgeInsets padding = MediaQuery.of(ctx).padding;
          return ColoredBox(
            color: Colors.transparent,
            child: Column(
              children: <Widget>[
                SizedBox(height: padding.top + 33),
                VideoCard(
                  video: video,
                  playVideo: true,
                  onLike: () {
                    onLike(ctx, video);
                  },
                  onDislike: () {
                    onDislike(ctx, video);
                  },
                ),
              ],
            ),
          );
        });
  }

  void onLike(BuildContext ctx, Video video) {
    if (!video.noMoreLikes) {
      ctx.read(videoPvdr).giveLike(video.id);
    } else {
      Scaffold.of(ctx).showSnackBar(
        SnackBar(
          content: Text("You can only like once"),
        ),
      );
    }
  }

  void onDislike(BuildContext ctx, Video video) {
    if (!video.noMoreDislikes) {
      ctx.read(videoPvdr).giveDislike(video.id);
    } else {
      Scaffold.of(ctx).showSnackBar(
        SnackBar(
          content: Text("You can only give I don't like once"),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    AsyncValue<List<Video>> videosState = useProvider(videoPvdr.state);
    VideoStateModel videoState = useProvider(videoStatePvdr.state);

    Size size = MediaQuery.of(context).size;
    double miniBarHeight = size.height * 0.075;

    return Scaffold(
      body: videosState.when(
        data: (List<Video> videoList) =>
            NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollNftn) {
            if (scrollNftn is ScrollUpdateNotification &&
                scrollNftn.metrics.atEdge) {
              if (scrollNftn.metrics.extentAfter < 10) {
                // We are at the end of the view
                context.read(videoStatePvdr).isLoading(true);
                context.read(videoPvdr).getVideos();
              }
            }
            return;
          },
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  primary: false,
                  children: <Widget>[
                    ...videoList
                        .map<Widget>(
                          (Video video) => VideoCard(
                            onTap: () {
                              context.read(videoStatePvdr).choseVideo(video);
                              showVideoDetail(context, video);
                            },
                            playVideo: false,
                            video: video,
                            onLike: () {
                              onLike(context, video);
                            },
                            onDislike: () {
                              onDislike(context, video);
                            },
                          ),
                        )
                        .toList(),
                    if (videoState.isLoading)
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Colors.white),
                          child: Center(
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(youtubeRed),
                            ),
                          ),
                        ),
                      ),
                    if (videoState.isLoading)
                      SingleChildScrollView(
                        child: VideoCardSkeleton(numItems: 3),
                      ),
                  ],
                ),
              ),
              if (videoState.videoSelected != null)
                SizedBox(
                  width: size.width,
                  height: miniBarHeight,
                  child: MiniBar(
                    video: videoState.videoSelected,
                    closeMiniBar: () {
                      context.read(videoStatePvdr).choseVideo(null);
                    },
                  ),
                ),
            ],
          ),
        ),
        loading: () =>
            SingleChildScrollView(child: VideoCardSkeleton(numItems: 3)),
        error: (Object err, StackTrace info) => Text('Error: $info'),
      ),
    );
  }
}
