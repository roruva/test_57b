part of views.youtube;

class SubscriptionsView extends StatelessWidget {
  const SubscriptionsView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ColoredBox(
      color: Colors.orange,
      child: SizedBox(
        width: size.width,
        height: size.height,
        child: Center(
          child: Icon(
            Icons.subscriptions_outlined,
            size: 96,
            color: youtubeBlack.withOpacity(0.2),
          ),
        ),
      ),
    );
  }
}
