library views.youtube;

/// [dart-packages]
import 'dart:ui';

/// [flutter-packages]
import 'package:flutter/material.dart';

/// [third-packages]
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:test_57b/components/components.dart';

/// [app-packages]
import 'package:test_57b/constants/constants.dart';
import 'package:test_57b/models/models.dart';
import 'package:test_57b/state/providers.dart';
import 'package:test_57b/state/video_state.dart';
import 'package:test_57b/utils/utils.dart';

/// [lib-parts]
part './home.dart';
part './explore.dart';
part './subscriptions.dart';
part './library.dart';
part './main.dart';
