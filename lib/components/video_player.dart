part of components;

class VideoPlayer extends StatefulWidget {
  const VideoPlayer({this.video, this.miniMode = false, Key key})
      : super(key: key);

  final Video video;
  final bool miniMode;

  @override
  _VideoPlayerState createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {
  YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: widget.video.id,
      flags: const YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true,
      ),
    );
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => YoutubePlayerBuilder(
        onExitFullScreen: () {
          // The player forces portraitUp after exiting fullscreen. This overrides the behaviour.
          SystemChrome.setPreferredOrientations(DeviceOrientation.values);
        },
        player: YoutubePlayer(
          controller: _controller,
          showVideoProgressIndicator: true,
          progressIndicatorColor: youtubeRed,
          topActions: !widget.miniMode
              ? <Widget>[
                  SizedBox(width: 8.0),
                  Expanded(
                    child: Text(
                      widget.video.title,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                ]
              : [],
          onReady: () {},
          onEnded: (data) {},
        ),
        builder: (context, player) => FittedBox(
          child: AspectRatio(aspectRatio: 16 / 9, child: player),
          alignment: Alignment.topCenter,
        ),
      );
}
