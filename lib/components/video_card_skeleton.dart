part of components;

class VideoCardSkeleton extends StatelessWidget {
  const VideoCardSkeleton({this.numItems, Key key}) : super(key: key);

  final int numItems;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SkeletonLoader(
      builder: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ColoredBox(
            color: Colors.grey,
            child: SizedBox(width: size.width, height: 202),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(10, 8, 16, 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                CircleAvatar(backgroundColor: Colors.white, radius: 18),
                SizedBox(width: 8),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ColoredBox(
                        color: Colors.grey,
                        child: SizedBox(height: 10, width: size.width),
                      ),
                      SizedBox(height: 4),
                      ColoredBox(
                        color: Colors.grey,
                        child: SizedBox(height: 10, width: size.width),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 4),
                ColoredBox(
                  color: Colors.grey,
                  child: SizedBox(height: 8, width: 8),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ColoredBox(
                color: Colors.grey,
                child: SizedBox(height: 24, width: 24),
              ),
              SizedBox(width: 16),
              ColoredBox(
                color: Colors.grey,
                child: SizedBox(height: 24, width: 24),
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ColoredBox(
                color: Colors.grey,
                child: SizedBox(height: 6, width: 24),
              ),
              SizedBox(width: 16),
              ColoredBox(
                color: Colors.grey,
                child: SizedBox(height: 6, width: 24),
              ),
            ],
          ),
          SizedBox(height: 16),
        ],
      ),
      items: numItems,
      period: Duration(milliseconds: 1500),
      highlightColor: Colors.grey[400],
      direction: SkeletonDirection.ltr,
    );
  }
}
