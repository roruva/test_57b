part of components;

class VideoCard extends StatelessWidget {
  const VideoCard({
    @required this.video,
    this.onLike,
    this.onDislike,
    this.onTap,
    this.playVideo = false,
    Key key,
  }) : super(key: key);

  final Video video;
  final bool playVideo;
  final void Function() onLike;
  final void Function() onDislike;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ColoredBox(
      color: Colors.white,
      child: SizedBox(
        width: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: onTap,
              child: ColoredBox(
                color: Colors.grey.withOpacity(0.4),
                child: SizedBox(
                  width: size.width,
                  height: video.thumbnails.height.toDouble(),
                  child: playVideo
                      ? VideoPlayer(video: video)
                      : Image.network(
                          video.thumbnails.url,
                          fit: BoxFit.cover,
                        ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10, bottom: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 18,
                    backgroundImage: NetworkImage(
                      video.channelThumb.url,
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          video.title,
                          maxLines: 2,
                          textAlign: TextAlign.left,
                          style: videoTitle,
                        ),
                        SizedBox(height: 4),
                        Text(
                          video.description,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: grey,
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.more_vert,
                      size: 18,
                    ),
                    onPressed: null,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10, bottom: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 32,
                        child: IconButton(
                          icon: Icon(
                            video.noMoreLikes
                                ? Icons.thumb_up
                                : Icons.thumb_up_outlined,
                            color: video.noMoreLikes ? youtubeBlack : grey,
                          ),
                          onPressed: onLike,
                        ),
                      ),
                      Text(
                        video.likesString,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: grey),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 32,
                        child: IconButton(
                          icon: Icon(
                            video.noMoreDislikes
                                ? Icons.thumb_down
                                : Icons.thumb_down_outlined,
                            color: video.noMoreDislikes ? youtubeBlack : grey,
                          ),
                          onPressed: onDislike,
                        ),
                      ),
                      Text(
                        '${video.dislikes}',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: grey),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 16),
          ],
        ),
      ),
    );
  }
}
