part of components;

class MiniBar extends StatelessWidget {
  const MiniBar({
    this.video,
    this.closeMiniBar,
    this.playVideo = true,
    this.showControls = true,
    Key key,
  }) : super(key: key);

  final Video video;
  final bool playVideo;
  final bool showControls;
  final Function() closeMiniBar;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double imageWidth = size.width * 0.3;
    return ColoredBox(
      color: Colors.white.withOpacity(0.8),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ColoredBox(
            color: Colors.grey.withOpacity(0.4),
            child: SizedBox(
              width: imageWidth,
              child: playVideo
                  ? VideoPlayer(video: video)
                  : Image.network(
                      video.thumbnails.url,
                      width: imageWidth,
                      fit: BoxFit.fitWidth,
                    ),
            ),
          ),
          SizedBox(width: 16),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(video.title, style: videoTitle),
                Text(video.owner, style: videoDesc),
              ],
            ),
          ),
          if (showControls) ...[
            IconButton(
              icon: Icon(Icons.play_arrow, size: 28),
              onPressed: closeMiniBar,
            ),
            IconButton(
              icon: Icon(Icons.close, size: 28),
              onPressed: closeMiniBar,
            )
          ]
        ],
      ),
    );
  }
}
