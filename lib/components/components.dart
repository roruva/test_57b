library components;

/// [flutter-packages]
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

/// [third-packages]
import 'package:skeleton_loader/skeleton_loader.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

/// [app-packages]
import 'package:test_57b/constants/constants.dart';
import 'package:test_57b/models/models.dart';
import 'package:test_57b/state/providers.dart';

/// [lib-parts]
part './video_card.dart';
part './video_card_skeleton.dart';
part './mini_bar.dart';
part './video_player.dart';
part './data_search.dart';
