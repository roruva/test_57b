part of components;

class DataSearch extends SearchDelegate {
  DataSearch({@required String label})
      : super(
          searchFieldLabel: label ?? '',
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.search,
        );

  Widget displayContent(BuildContext context, List<Video> list) => ColoredBox(
        color: Colors.white,
        child: ListView.builder(
          padding: EdgeInsets.all(8),
          shrinkWrap: true,
          itemBuilder: (BuildContext ctx, int index) => Column(
            children: <Widget>[
              InkResponse(
                onTap: Navigator.of(ctx).pop,
                child: MiniBar(
                  playVideo: false,
                  showControls: false,
                  video: list[index],
                  closeMiniBar: () {},
                ),
              ),
              SizedBox(height: 8),
            ],
          ),
          itemCount: list.length,
        ),
      );

  Widget msgResult(String message) => Center(
        child: Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 20,
            color: youtubeBlack,
            letterSpacing: 0,
          ),
        ),
      );

  @override
  List<Widget> buildActions(BuildContext context) => <Widget>[
        IconButton(
          icon: Icon(Icons.backspace),
          onPressed: () {
            query = '';
          },
        ),
      ];

  @override
  Widget buildLeading(BuildContext context) => IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      );

  @override
  Widget buildResults(BuildContext context) {
    return SizedBox();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.length < 2) {
      return msgResult('Search term must be longer than two letters.');
    }
    return Consumer(builder: (context, watch, child) {
      AsyncValue<List<Video>> videosState = watch(videosFuturePvdr(query));

      return videosState.when(
        data: (List<Video> videos) {
          return displayContent(context, videos);
        },
        loading: () {
          return Center(child: CircularProgressIndicator());
        },
        error: (Object error, StackTrace stackTrace) =>
            msgResult('We found no results for your search'),
      );
    });
  }
}
