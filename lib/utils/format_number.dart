part of utils;

String formatNum(num number) {
  if (number > 9999) {
    List<String> numString = '${number / 1000}'.split('.');
    return '${numString[0]}.${numString[1].substring(0, 2)} K';
  } else {
    return '$number';
  }
}
