part of utils;

class Requester {
  static Future<List<Video>> fetchPage(int pageSize, int pageNumber) async {
    int delay = Random().nextInt(3);
    await Future.delayed(Duration(seconds: delay));

    String jsonString =
        await rootBundle.loadString('assets/data/mocked_videos.json');
    List<dynamic> jsonParsed = jsonDecode(jsonString) as List;

    List<Video> videos = jsonParsed
        .map((dynamic videoJson) => Video.fromJson(videoJson))
        .toList()
        .getRange(pageSize * pageNumber, ((pageNumber + 1) * pageSize) - 1)
        .toList();

    return videos;
  }

  static Future<List<Video>> searchVideos(String query) async {
    int delay = Random().nextInt(3);
    await Future.delayed(Duration(seconds: delay));

    String jsonString =
        await rootBundle.loadString('assets/data/mocked_videos.json');
    List<dynamic> jsonParsed = jsonDecode(jsonString) as List;

    List<Video> videos = jsonParsed
        .map((dynamic videoJson) => Video.fromJson(videoJson))
        .where((Video video) =>
            video.title.toLowerCase().contains(query.toLowerCase()))
        .toList();

    return videos;
  }
}
